# Project Title

A Python package for exploring chromatin architecture through a random walk (Markov State Model) analysis of Hi-C data.

** NOTE: This readme is under construction. **

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Deployment on a server should require just the same steps.

### Prerequisites

This library is built for Python 2.7.

List of required Python libraries (versions):
* Numpy (1.14)
* Scipy (1.0)
* Pandas (0.22)
* Matplotlib (2.0)

### Installing

1. Install the SciPy stack

The most convenient way is to use pip:

```
python -m pip install --user numpy scipy matplotlib pandas
```

2. Copy package source to Python path
3. Basic usage of the package can be inferred by looking at the code that's run when you execute ChromaWalker.py as a main program. In other words, look at how the program is run in ChromaWalker.py, under the conditional statement:

```
if __name__ == �__main__�:
```

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Zhen Wah Tan** - *Initial work* - [ZhenWahTan](https://bitbucket.org/ZhenWahTan/)

See also the list of [contributors](https://bitbucket.org/ZhenWahTan/chromawalker/contributors) who participated in this project.

## License

...

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

